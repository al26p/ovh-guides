# OVHcloud Guides

Just some fork from [OVHcloud docs](https://github.com/ovh/docs/).
Find all docs at [help.ovhcloud.com](https://help.ovhcloud.com/csm/world-documentation-public-cloud?id=kb_browse_cat&kb_id=574a8325551974502d4c6e78b7421938)


This repo is intended to help with workshops organised for OVHcloud.